tetris : menu.o sauvegarde.o score.o pseudo.o piece.o afficher_plateau.o initialisation_plateau.o ./src/main.c
	gcc -W -O3 -Wall `pkg-config --cflags MLV` `pkg-config --libs-only-other --libs-only-L MLV` ./src/main.c menu.o sauvegarde.o score.o pseudo.o initialisation_plateau.o afficher_plateau.o piece.o `pkg-config --libs-only-l MLV` -o tetris

initialisation_plateau.o : ./src/initialisation_plateau.c ./include/initialisation_plateau.h
	gcc -c -W -Wall -O ./src/initialisation_plateau.c

afficher_plateau.o : ./src/afficher_plateau.c ./include/afficher_plateau.h
	gcc -c -W -Wall -O ./src/afficher_plateau.c

piece.o : ./src/piece.c ./include/piece.h
	gcc -c -W -Wall -O ./src/piece.c

pseudo.o : ./src/pseudo.c ./include/pseudo.h
	gcc -c -W -Wall -O ./src/pseudo.c

score.o : ./src/score.c ./include/score.h
	gcc -c -W -Wall -O ./src/score.c

sauvegarde.o : ./src/sauvegarde.c ./include/sauvegarde.h
	gcc -c -W -Wall -O ./src/sauvegarde.c

menu.o : ./src/menu.c ./include/menu.h
	gcc -c -W -Wall -O ./src/menu.c
clean:
	rm -f tetris *.o
