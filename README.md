# Tetris


![Tetris](img/menu1.png)


Projet langage C avec MLV.
http://www-igm.univ-mlv.fr/~boussica/mlv/api/French/html/index.html

Revenir en arrière dans les menus : ECHAP
Valider dans les menus : ESPACE
Naviguer dans les menus : avec les flèches
Rotation des pièces : Z

make pour compiler
make clean pour supprimer les fichiers
