#include <MLV/MLV_all.h>
#include <string.h>

/* Fonction qui demande le pseudo du joueur avec une boite d'écriture */
char* ask_pseudo(){
  char* pseudo;
  MLV_wait_input_box(200,425,450,40,MLV_COLOR_WHITE, MLV_COLOR_WHITE, MLV_COLOR_BLACK,"",&pseudo);
  return pseudo;
}

/* Fonction qui retourne 1 si le pseudo est bon, -1 sinon */
int verification_pseudo(char* pseudo){
  if(strcmp(pseudo,"") == 0)
    return -1;
  else return 1;
}
