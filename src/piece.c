#include "../include/structure.h"
#include <stdlib.h>
#include <stdio.h>
#include <MLV/MLV_all.h>
#include <time.h>






type_piece piece_alea (){
  type_piece pieceT,pieceZ2,pieceZ1,pieceLIGNE,pieceO,pieceL1,pieceL2,piece_aleatoire;
  int num_piece_alea;
  /* Chargement des pièces */


  /*
0000
1111
0000
0000
  */  
  pieceLIGNE.piece[0][0]=0;
  pieceLIGNE.piece[0][1]=0;
  pieceLIGNE.piece[0][2]=0;
  pieceLIGNE.piece[0][3]=0;
  pieceLIGNE.piece[1][0]=1;
  pieceLIGNE.piece[1][1]=1;
  pieceLIGNE.piece[1][2]=1;
  pieceLIGNE.piece[1][3]=1;
  pieceLIGNE.piece[2][0]=0;
  pieceLIGNE.piece[2][1]=0;
  pieceLIGNE.piece[2][2]=0;
  pieceLIGNE.piece[2][3]=0;
  pieceLIGNE.piece[3][0]=0;
  pieceLIGNE.piece[3][1]=0;
  pieceLIGNE.piece[3][2]=0;
  pieceLIGNE.piece[3][3]=0;

    /*
0110
0110
0000
0000
  */  
  pieceO.piece[0][0]=0;
  pieceO.piece[0][1]=2;
  pieceO.piece[0][2]=2;
  pieceO.piece[0][3]=0;
  pieceO.piece[1][0]=0;
  pieceO.piece[1][1]=2;
  pieceO.piece[1][2]=2;
  pieceO.piece[1][3]=0;
  pieceO.piece[2][0]=0;
  pieceO.piece[2][1]=0;
  pieceO.piece[2][2]=0;
  pieceO.piece[2][3]=0;
  pieceO.piece[3][0]=0;
  pieceO.piece[3][1]=0;
  pieceO.piece[3][2]=0;
  pieceO.piece[3][3]=0;
     /*
1000
1000
1100
0000
  */
  pieceL1.piece[0][0]=3;
  pieceL1.piece[0][1]=0;
  pieceL1.piece[0][2]=0;
  pieceL1.piece[0][3]=0;
  pieceL1.piece[1][0]=3;
  pieceL1.piece[1][1]=3;
  pieceL1.piece[1][2]=3;
  pieceL1.piece[1][3]=0;
  pieceL1.piece[2][0]=0;
  pieceL1.piece[2][1]=0;
  pieceL1.piece[2][2]=0;
  pieceL1.piece[2][3]=0;
  pieceL1.piece[3][0]=0;
  pieceL1.piece[3][1]=0;
  pieceL1.piece[3][2]=0;
  pieceL1.piece[3][2]=0;
    /*
0010
0010
0110
0000
  */
  pieceL2.piece[0][0]=0;
  pieceL2.piece[0][1]=0;
  pieceL2.piece[0][2]=4;
  pieceL2.piece[0][3]=0;
  pieceL2.piece[1][0]=4;
  pieceL2.piece[1][1]=4;
  pieceL2.piece[1][2]=4;
  pieceL2.piece[1][3]=0;
  pieceL2.piece[2][0]=0;
  pieceL2.piece[2][1]=0;
  pieceL2.piece[2][2]=0;
  pieceL2.piece[2][3]=0;
  pieceL2.piece[3][0]=0;
  pieceL2.piece[3][1]=0;
  pieceL2.piece[3][2]=0;
  pieceL2.piece[3][3]=0;
    /*
0110
1100
0000
0000
  */
  pieceZ1.piece[0][0]=0;
  pieceZ1.piece[0][1]=5;
  pieceZ1.piece[0][2]=5;
  pieceZ1.piece[0][3]=0;
  pieceZ1.piece[1][0]=5;
  pieceZ1.piece[1][1]=5;
  pieceZ1.piece[1][2]=0;
  pieceZ1.piece[1][3]=0;
  pieceZ1.piece[2][0]=0;
  pieceZ1.piece[2][1]=0;
  pieceZ1.piece[2][2]=0;
  pieceZ1.piece[2][3]=0;
  pieceZ1.piece[3][0]=0;
  pieceZ1.piece[3][1]=0;
  pieceZ1.piece[3][2]=0;
  pieceZ1.piece[3][3]=0;
    /*
0110
0011
0000
0000
  */
  pieceZ2.piece[0][0]=6;
  pieceZ2.piece[0][1]=6;
  pieceZ2.piece[0][2]=0;
  pieceZ2.piece[0][3]=0;
  pieceZ2.piece[1][0]=0;
  pieceZ2.piece[1][1]=6;
  pieceZ2.piece[1][2]=6;
  pieceZ2.piece[1][3]=0;
  pieceZ2.piece[2][0]=0;
  pieceZ2.piece[2][1]=0;
  pieceZ2.piece[2][2]=0;
  pieceZ2.piece[2][3]=0;
  pieceZ2.piece[3][0]=0;
  pieceZ2.piece[3][1]=0;
  pieceZ2.piece[3][2]=0;
  pieceZ2.piece[3][3]=0;
   /*
0000
0111
0010
0000
  */
  pieceT.piece[0][0]=0;
  pieceT.piece[0][1]=0;
  pieceT.piece[0][2]=0;
  pieceT.piece[0][3]=0;
  pieceT.piece[1][0]=9;
  pieceT.piece[1][1]=9;
  pieceT.piece[1][2]=9;
  pieceT.piece[1][3]=0;
  pieceT.piece[2][0]=0;
  pieceT.piece[2][1]=9;
  pieceT.piece[2][2]=0;
  pieceT.piece[2][3]=0;
  pieceT.piece[3][0]=0;
  pieceT.piece[3][1]=0;
  pieceT.piece[3][2]=0;
  pieceT.piece[3][3]=0;

  /*choisie une piece aleatoire est la met dans piece_alea*/
  num_piece_alea=rand()%(7-1+1)+1;
  switch(num_piece_alea){
  case 1 :
    piece_aleatoire = pieceT;
    break;
  case 2 :
    piece_aleatoire = pieceZ2;
    break;
  case 3 :
    piece_aleatoire = pieceZ1;
    break;
  case 4 :
    piece_aleatoire = pieceLIGNE;
    break;
  case 5 :
    piece_aleatoire = pieceO;
    break;
  case 6 :
    piece_aleatoire = pieceL1;
    break;
  case 7 :
    piece_aleatoire = pieceL2;
    break;
 
  }
  return piece_aleatoire;
}

void placer_piece(type_piece piece,type_plateau *plateau,int pos_x, int pos_y){
  int i,j,ip=0,jp=0;/*ip et jp sont les indoce de la piece a placer*/
  /*enleve les bloque dans la zone morte*/
  for(i=0;i<4;i++){
    for(j=1;j<10;j++){
      plateau->plateau[i][j]=0;
    
    }
  }
    /*place la piece dans la zone */
    for(i=pos_y-4;i<pos_y;i++){
      for(j=pos_x+1;j<pos_x+5;j++){
	if(piece.piece[ip][jp]!=0){
	  plateau->plateau[i][j] = piece.piece[ip][jp];
	}
	jp+=1;
      }
      jp=0;
      ip+=1;
    }
  }


int mouvement_piece(type_piece piece, type_plateau *plateau,int position, MLV_Keyboard_button touche,int position_y){
  int i;
  int y1d=5,y2d=5,y3d=5,y4d=5;/*chaque position du block le plus a droite pour chaque ligne*/
  int y1g=5,y2g=5,y3g=5,y4g=5;;/*chaque position du block le plus a gauche pour chaque ligne*/
  /*prend les indice pour chaque ligne du bloque tout à droite et le met dans yid*/
  for(i=0;i<4;i++){
    if(piece.piece[0][i]!=0){
      y1d=i;
    }
    if(piece.piece[1][i]!=0){
      y2d=i;
    }
      
    if(piece.piece[2][i]!=0){
      y3d=i;
    }
    if(piece.piece[3][i]!=0){
      y4d=i;
    }
  }
  /*prend les indice pour chaque ligne du bloque tout à gauche et le met dans yig*/
  for(i=3;i>=0;i--){
    if(piece.piece[0][i]!=0){
      y1g=i;
    }
    if(piece.piece[1][i]!=0){
      y2g=i;
    }
      
    if(piece.piece[2][i]!=0){
      y3g=i;
    }
    if(piece.piece[3][i]!=0){
      y4g=i;
    }
  }
  /*verifie si le bloque en bas a droite de chaque bloque a droite de la piece puis determine la position*/
  if(touche==MLV_KEYBOARD_RIGHT){
    if(((plateau->plateau[position_y][position+y1d+2]!= 0) && (piece.piece[0][y1d]!=0) && (y1d!=5)) ||
       ((plateau->plateau[position_y-1][position+y2d+2]!= 0) && (piece.piece[1][y2d]!=0) && (y2d!=5)) ||
       ((plateau->plateau[position_y-2][position+y3d+2]!= 0) && (piece.piece[2][y3d]!=0) && (y3d!=5)) ||
       ((plateau->plateau[position_y-3][position+y4d+2]!= 0) && (piece.piece[3][y4d]!=0) && (y4d!=5)) ){
      position=position;
    }else{
      position=position+1;
    }
    /*affiche la piece a la position*/
    placer_piece(piece,plateau,position,4);
  }
    
  /*verifie si le bloque en bas a gauche de chaque bloque a gauche de la piece puis determine la position*/
  if(touche==MLV_KEYBOARD_LEFT){
    if(((plateau->plateau[position_y][position+y1g]!= 0) && (piece.piece[0][y1g]!=0) && (y1g!=5)) ||
       ((plateau->plateau[position_y-1][position+y2g]!= 0) && (piece.piece[1][y2g]!=0) && (y2g!=5)) ||
       ((plateau->plateau[position_y-2][position+y3g]!= 0) && (piece.piece[2][y3g]!=0) && (y3g!=5)) ||
       ((plateau->plateau[position_y-3][position+y4g]!= 0) && (piece.piece[3][y4g]!=0) && (y4g!=5)) ){
      position=position;
    }else{
      position=position-1;
    }
    /*affiche la piece a ka position*/
    placer_piece(piece,plateau,position,4);
  }


  return position;
}

type_piece rotation_piece(type_piece piece, type_plateau *plateau, int pos_x, int pos_y){
  if(plateau->plateau[pos_y][pos_x+1] == 0 &&
     plateau->plateau[pos_y][pos_x+2] == 0 &&
     plateau->plateau[pos_y][pos_x+3] == 0 &&
     plateau->plateau[pos_y][pos_x+4] == 0){
    /*cas oou la piece est pieceO*/
    if(piece.piece[0][1]==2){
      return piece;
    }
    /*cas oou la piece est pieceLigne*/
    if(piece.piece[1][2]==1){
      if(piece.piece[1][1]==1){
	piece.piece[1][0]=0;
	piece.piece[1][1]=0;
	piece.piece[1][3]=0;
	piece.piece[0][2]=1;
	piece.piece[2][2]=1;
	piece.piece[3][2]=1;
	return piece;
      }
      else
	{
	  
	  piece.piece[1][0]=1;
	  piece.piece[1][1]=1;
	  piece.piece[1][3]=1;
	  piece.piece[0][2]=0;
	  piece.piece[2][2]=0;
	  piece.piece[3][2]=0;
	  return piece;
	}
    }

  
    /*cas oou la piece est pieceL1*/
    if(piece.piece[1][1]==3){
      if(piece.piece[0][0]==3){
	piece.piece[0][0]=0;
	piece.piece[1][0]=0;
	piece.piece[1][2]=0;
	piece.piece[0][1]=3;
	piece.piece[0][2]=3;
	piece.piece[2][1]=3;
	return piece;
      }
      if(piece.piece[0][2]==3){
	piece.piece[0][1]=0;
	piece.piece[0][2]=0;
	piece.piece[2][1]=0;
	piece.piece[1][0]=3;
	piece.piece[1][2]=3;
	piece.piece[2][2]=3;
	return piece;
      }
      if(piece.piece[2][2]==3){
	piece.piece[0][1]=3;
	piece.piece[2][1]=3;
	piece.piece[2][0]=3;
	piece.piece[2][2]=0;
	piece.piece[1][2]=0;
	piece.piece[1][0]=0;
	return piece;
      }
      if(piece.piece[2][0]==3){
	piece.piece[0][1]=0;
	piece.piece[2][1]=0;
	piece.piece[2][0]=0;
	piece.piece[0][0]=3;
	piece.piece[1][0]=3;
	piece.piece[1][2]=3;
	return piece;
      }
    }
    /*cas oou la piece est pieceL2*/
    if(piece.piece[1][1]==4){
      if(piece.piece[0][0]==4){
	piece.piece[0][0]=0;
	piece.piece[0][1]=0;
	piece.piece[2][1]=0;
	piece.piece[1][2]=4;
	piece.piece[0][2]=4;
	piece.piece[1][0]=4;
	return piece;
      }
      if(piece.piece[0][2]==4){
	piece.piece[1][2]=0;
	piece.piece[0][2]=0;
	piece.piece[1][0]=0;
	piece.piece[0][1]=4;
	piece.piece[2][1]=4;
	piece.piece[2][2]=4;
	return piece;
      }
      if(piece.piece[2][2]==4){
	piece.piece[0][1]=0;
	piece.piece[2][1]=0;
	piece.piece[2][2]=0;
	piece.piece[1][0]=4;
	piece.piece[2][0]=4;
	piece.piece[1][2]=4;
	return piece;
      }
      if(piece.piece[2][0]==4){
	piece.piece[1][0]=0;
	piece.piece[2][0]=0;
	piece.piece[1][2]=0;
	piece.piece[0][0]=4;
	piece.piece[0][1]=4;
	piece.piece[2][1]=4;
	return piece;
      }
    }

    /*cas oou la piece est pieceT*/
    if(piece.piece[1][1]==9){
      if(piece.piece[1][0]==9 &&
	 piece.piece[2][1]==9 &&
	 piece.piece[1][2]==9 ){
      
	piece.piece[1][2]=0;
	piece.piece[0][1]=9;
      
	return piece;
      }
      if(piece.piece[1][0]==9 &&
	 piece.piece[2][1]==9 &&
	 piece.piece[0][1]==9 ){
      
	piece.piece[2][1]=0;
	piece.piece[1][2]=9;
      
	return piece;
      }
      if(piece.piece[1][0]==9 &&
	 piece.piece[0][1]==9 &&
	 piece.piece[1][2]==9 ){
      
	piece.piece[1][0]=0;
	piece.piece[2][1]=9;
      
	return piece;
      }
      if(piece.piece[0][1]==9 &&
	 piece.piece[2][1]==9 &&
	 piece.piece[1][2]==9 ){
      
	piece.piece[1][0]=9;
	piece.piece[0][1]=0;
      
	return piece;
      }
    }
    /*cas oou la piece est pieceZ1*/
    if(piece.piece[1][1]==5){
      if(piece.piece[0][2]==5){
	piece.piece[0][2]=0;
	piece.piece[0][1]=0;
	piece.piece[2][1]=5;
	piece.piece[0][0]=5;
	return piece;
      }
      if(piece.piece[0][0]==5){
	piece.piece[0][2]=5;
	piece.piece[0][1]=5;
	piece.piece[2][1]=0;
	piece.piece[0][0]=0;
	return piece;
      }
      return piece;
    }
    /*cas oou la piece est pieceZ2*/
    if(piece.piece[1][1]==6){
      if(piece.piece[0][2]==6){
	piece.piece[0][2]=0;
	piece.piece[2][1]=0;
	piece.piece[0][1]=6;
	piece.piece[0][0]=6;
	return piece;
      }
      if(piece.piece[0][0]==6){
	piece.piece[0][2]=6;
	piece.piece[2][1]=6;
	piece.piece[0][1]=0;
	piece.piece[0][0]=0;
	return piece;
      }
    }
  }
  return piece;
}

void supprimer_piece(type_piece piece,type_plateau *plateau, int pos_x, int pos_y){
  int i,j;
  /*grace au position x et y de la piece cette fonction supprime la piece */
  for(i=0;i<4;i++){
    for(j=0;j<4;j++){
      if(piece.piece[j][i]!=0){
	plateau->plateau[j+pos_y-4][i+pos_x+1]=0;
       
      }

    }
  }
}

int descendre_piece(type_piece piece , type_plateau *plateau,int pos_x,int score){
  int pos_y=4,n=0,i,z=0;/*n= condition d'arret dans le while*/  
  int x1=5,x2=5,x3=5,x4=5;/*position du block le plus bas pour chaque cologne de la matrice piece*/
  /*cherche les bloque de la piece les plus bas pour chaque colonne de la matrice piece*/
  for(i=0;i<4;i++){
    if(piece.piece[i][0]!=0){
      x1=i;
    }
    if(piece.piece[i][1]!=0){
      x2=i;
    }
    if(piece.piece[i][2]!=0){
      x3=i;
    }
    if(piece.piece[i][3]!=0){
      x4=i;
    }
  }
  /*si les deux dernier ligne de la matrice de la piece sont vide*/
  if(piece.piece[2][0]==0 && piece.piece[2][1]==0 && piece.piece[2][2]==0 && piece.piece[2][3]==0 && piece.piece[3][0]==0 && piece.piece[3][1]==0 && piece.piece[3][2]==0 && piece.piece[3][3]==0){
    pos_y=2;/*changement de la pos_y car les 2 derniere ligne de la matrice piece sont vide*/
    while(n==0){
      /*verifie pour chaque colonne si le bloque en dessus du bloque le plus bas pour chaque colone est plein */
      if( ((plateau->plateau[x1+z+1][pos_x+1] !=0) && (piece.piece[x1][0] != 0) && (x1!=5)) ||
	  ((plateau->plateau[x2+z+1][pos_x+2] !=0) && (piece.piece[x2][1] != 0) && (x2!=5)) ||
	  ((plateau->plateau[x3+z+1][pos_x+3] !=0) && (piece.piece[x3][2] != 0) && (x3!=5)) ||
	  ((plateau->plateau[x4+z+1][pos_x+4] !=0) && (piece.piece[x4][3] != 0) && (x4!=5)) ){
	n=1;
      }else{
	pos_y++;
	z++;
      }
      
    }
    placer_piece(piece, plateau,pos_x,pos_y+2);
  }else{
     /*si la dernier ligne de la matrice de la piece sont vide*/
    if(piece.piece[3][0]==0 && piece.piece[3][1]==0 && piece.piece[3][2]==0 && piece.piece[3][3]==0){
      pos_y=3;/*changement de la pos_y car la derniere ligne de la matrice piece sont vide*/
      n=0;
      while(n==0){
 /*verifie pour chaque colonne si le bloque en dessus du bloque le plus bas pour chaque colone est plein */
	if( ((plateau->plateau[x1+z+1][pos_x+1] !=0) && (piece.piece[x1][0] != 0) && (x1!=5 )) ||
	    ((plateau->plateau[x2+z+1][pos_x+2] !=0) && (piece.piece[x2][1] != 0) && (x2!=5 )) ||
	    ((plateau->plateau[x3+z+1][pos_x+3] !=0) && (piece.piece[x3][2] != 0) && (x3!=5 )) ||
	    ((plateau->plateau[x4+z+1][pos_x+4] !=0) && (piece.piece[x4][3] != 0) && (x4!=5 )) ){
	  n=1;
	}else{
	  pos_y++;
	  z++;
	}
      }
      placer_piece(piece,plateau,pos_x,pos_y+1);
    }else{
      /*si la matrice de la piece utilise sa dernier ligne*/
      pos_y=4;
      n=0;
      while(n==0){
	 /*verifie pour chaque colonne si le bloque en dessus du bloque le plus bas pour chaque colone est plein */
	if( ((plateau->plateau[x1+z+1][pos_x+1] !=0) && (piece.piece[x1][0] != 0)  && (x1!=5)) ||
	    ((plateau->plateau[x2+z+1][pos_x+2] !=0) && (piece.piece[x2][1] != 0)  && (x2!=5)) ||
	    ((plateau->plateau[x3+z+1][pos_x+3] !=0) && (piece.piece[x3][2] != 0)  && (x3!=5)) ||
	    ((plateau->plateau[x4+z+1][pos_x+4] !=0) && (piece.piece[x4][3] != 0)  && (x4!=5)) ){
	  n=1;
	}else{
	  pos_y++;
	  z++;
	}
      }
      placer_piece(piece,plateau,pos_x,pos_y);
    }
  
  }
  score+=5;
  return score;
}


int descendre_jeu(type_plateau *plateau,int score){
  int i,j,n=0,jp,ip;/*ip et jp sont les indices de la prochiane ligne pour pouvoir descendre tout le haut du jeu*/
  /* increment la variable n si un bloque de la,ligne n'est pas vide*/
  for(i=21;i>4;i--){
    for(j=1;j<10;j++){
      if(plateau->plateau[i][j]!=0){
	n++;
      }
    }
    /* si n=9 alors la ligne est pleine alors supprime cette ligne et descend le jeu et augmante le score*/
    if(n==9){
      score+=20;
      for(jp=1;jp<10;jp++){
	plateau->plateau[i][jp]=0;
      }
      for(ip=i;ip>4;ip--){
	for(jp=1;jp<10;jp++){
	  plateau->plateau[ip][jp]=plateau->plateau[ip-1][jp];
	}
      }	
      i++;
    }
    n=0;
  }
  return score;
}



int game_over(type_plateau *plateau){
  int i;
  /*si la ligne la plus basse de la zone morte contient un bloque alors le jeu s'arrete*/
  for(i=1;i<10;i++){
    if(plateau->plateau[3][i]!=0){
      return 1;
    }
  }
  return 0;
}
  
void afficher_pro_piece(type_piece piece){
  int i,j,x=587,y=170;/*x et y sont les position de la zone prochaine piece*/
  /*dans la zone prévu pour affiche la prochaine piecee a venir*/
  for(i=0;i<4;i++){
    for(j=0;j<4;j++){
      if(piece.piece[j][i]==8){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_RED);
      }
      if(piece.piece[j][i]==1){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_BLUE);
      }
      if(piece.piece[j][i]==2){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_GREEN);
      }
      if(piece.piece[j][i]==3){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_YELLOW);
      }
      if(piece.piece[j][i]==4){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_PINK);
      }
      if(piece.piece[j][i]==5){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_ORANGE);
      }
      if(piece.piece[j][i]==6){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_BROWN);
      }
      if(piece.piece[j][i]==9){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_PURPLE);
      }
    
      y+=31;

    }
    y=170;
    x+=31;

  }
}
