#include <stdlib.h>
#include <stdio.h>
#include <MLV/MLV_all.h>


/* Initialisation du menu de départ */
int initialisation_menu(MLV_Image *image_menu1){
    MLV_draw_image(image_menu1, 0, 0);
    MLV_actualise_window();
    return 4; /* Retourne la position du menu initial */
}

/* Fonction qui affiche une image en paramètre */
void affichage_image(MLV_Image *image){
    MLV_draw_image(image,0,0);
    MLV_actualise_window();
}