#define NBMAX 25
#define VIDE 0
#define BLOCK 1
#define MUR 7
#define HAUT 23
#define LONG 11
#include "../include/structure.h"


/*creation du plateau avec les murs et la zone morte*/
type_plateau creer_plateau(int hauteur, int longueur){
  type_plateau plateau_fct;
  int i,j;/*indice du plateau*/
  /* place les block mur*/
  for(i=0;i<hauteur;i++){
    for(j=0;j<longueur;j++){
      if (j==0 || j == longueur-1 || i==hauteur-1) {
        plateau_fct.plateau[i][j] = MUR;
      } else
	plateau_fct.plateau[i][j] = VIDE;
    }
  }
  /*place les blocks de la zone morte*/
  plateau_fct.plateau[0][0] = 11;
  plateau_fct.plateau[0][10] = 11;
  plateau_fct.plateau[1][0] = 11;
  plateau_fct.plateau[1][10] = 11;
  plateau_fct.plateau[2][0] = 11;
  plateau_fct.plateau[2][10] = 11;
  plateau_fct.plateau[3][0] = 11;
  plateau_fct.plateau[3][10] = 11;
  return plateau_fct;
}
