#include "../include/structure.h"
#include <stdlib.h>
#include <stdio.h>
#include <MLV/MLV_all.h>

void afficher_plateau(type_plateau plateau, int longueur, int hauteur){
  int i,j;
  for(i=0;i<longueur;i++){
    for(j=0;j<hauteur;j++){
      printf("%d",plateau.plateau[i][j]);
    }
    printf("\n");
  }
}
/*affiche la plateau avec MLV*/
void afficher_plateauMLV(type_plateau plateau, int longueur, int hauteur, char* pseudo,int score){
  int i,j;/*indice dans le plateau pour créer les blocks de différentes couleurs qui depend de l'entier dans la matrice plateau*/
  int x=133,y=136;/*position du plateau dans la fenetre de jeu*/
  char scoretext[20];
  for(i=0;i<hauteur;i++){
    for(j=0;j<longueur;j++){
      if(plateau.plateau[j][i]==8){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_RED);
      }
      if(plateau.plateau[j][i]==0){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_GRAY20);
      }
      if(plateau.plateau[j][i]==1){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_BLUE);
      }
      if(plateau.plateau[j][i]==2){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_GREEN);
      }
      if(plateau.plateau[j][i]==3){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_YELLOW);
      }
      if(plateau.plateau[j][i]==4){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_PINK);
      }
      if(plateau.plateau[j][i]==5){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_ORANGE);
      }
      if(plateau.plateau[j][i]==6){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_BROWN);
      }
      if(plateau.plateau[j][i]==9){
	MLV_draw_filled_rectangle(x,y,30,30,MLV_COLOR_PURPLE);
      }
      y+=31;

    }
    y=136;
    x+=31;

  }
  /* Affiche le pseudo */
  MLV_draw_text(670, 580,pseudo,MLV_COLOR_WHITE);
  /* score */
  sprintf(scoretext, "%d", score);
  MLV_draw_text(700, 440, scoretext,MLV_COLOR_WHITE);
  MLV_actualise_window();


}
