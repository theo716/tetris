#include <stdlib.h>
#include <stdio.h>
#include <MLV/MLV_all.h>
#include <time.h>
#include "../include/structure.h"
#include "../include/initialisation_plateau.h"
#include "../include/afficher_plateau.h"
#include "../include/piece.h"
#include "../include/pseudo.h"
#include "../include/score.h"
#include "../include/sauvegarde.h"
#include "../include/menu.h"

#define NBMAX 25
#define HAUT 23
#define LONG 11
#define TRUE 1
#define FALSE 0
#define FRAME 20

int main()
{
  /* Initialisation du module audio */
  //MLV_init_audio();

  /* Définition des variables */
  type_plateau plateau;
  int position_depart = 3;
  int etat_boucle_jeu = FALSE;
  int etat_boucle_menu = TRUE;
  int etat_boucle_pseudo = FALSE;
  int etat_boucle_score = FALSE;
  int etat_boucle_theme = FALSE;
  int verification_sauvegarde = FALSE;

  /* informations conernant les évènements de typeclavier */
  MLV_Keyboard_button sym, symp;
  MLV_Button_state state;

  /* Chargement de la musique */
  //MLV_Music *music = MLV_load_music("kahoot.mp3");

  /* Variable contenant le code associé au type d'un évènement.*/
  int position_menu = 5;
  int position_theme = 2;
  int score = 0;
  int vitesse = 5, images = 1;
  int pos_plat_y = 4;
  int n = 0, i,j;
  int x1 = 5, x2 = 5, x3 = 5, x4 = 5;/*les positions des blocks les plus bas pour chaque colonnes de la matrice piece*/
  int z = 0;/* condition d'arret de la boucle descendre piecee*/
  char pseudo_joueur[NBMAXPSEUDO];
  type_piece piece_aleatoire, prochaine_piece;
  MLV_Keyboard_button touche_menu, touche_menu_theme;
  MLV_Image *image_menu1, *image_menu2, *image_menu3, *image_menu4, *image_pseudo, *image_score;
  MLV_Image *image_theme1, *image_theme2, *image_theme_base, *image_theme_minecraft, *image_theme_selec, *image_pause;

  FILE *fichier;
  FILE *fichier_sauvegarde;
  srand(time(NULL));

  /* Création de la fenêtre de jeu */
  MLV_create_window("tetris", "tetris", 902, 1000);

  /* Chargement des images */
  image_menu1 = MLV_load_image("./img/menu1.png");
  image_menu2 = MLV_load_image("./img/menu2.png");
  image_menu3 = MLV_load_image("./img/menu3.png");
  image_menu4 = MLV_load_image("./img/menu4.png");
  image_pseudo = MLV_load_image("./img/pseudo.png");
  image_score = MLV_load_image("./img/menu_score.png");

  image_theme1 = MLV_load_image("./img/menu_theme1.png");
  image_theme2 = MLV_load_image("./img/menu_theme2.png");

  image_theme_base = MLV_load_image("./img/themebase.png");
  image_theme_minecraft = MLV_load_image("./img/theme_minecraft.png");
  image_theme_selec = MLV_load_image("./img/themebase.png");
  image_pause = MLV_load_image("./img/menu_pause.png");

  /* Initialisation du menu */
  position_menu = initialisation_menu(image_menu1);

  /* ==============================
     Boucle qui gère le menu
     ==============================*/
  while (etat_boucle_menu == TRUE)
    {
      /* ------------------------
	 Navigation dans le menu
	 ------------------------*/
      /* On colle la première image */
      if (position_menu % 4 == 0)
	affichage_image(image_menu1);
      /* On colle la deuxième image */
      if (position_menu % 4 == 1)
	affichage_image(image_menu2);
      /* On colle la troisième image */
      if (position_menu % 4 == 2)
	affichage_image(image_menu3);
      /* On colle la quatrième image */
      if (position_menu % 4 == 3)
	affichage_image(image_menu4);

      /* Attends la prochaine touche de l'utilisateur */
      MLV_wait_keyboard(&touche_menu, NULL, NULL);
      /* si touche S alors on descends dans le menu */
      if (touche_menu == MLV_KEYBOARD_DOWN)
		position_menu = position_menu + 1;
      /* si touche Z alors on monte dans le menu*/
      if (touche_menu == MLV_KEYBOARD_UP)
	{
	  /* si on passe dans le négatif, on reset le compteur */
	  if (position_menu == 0)
	    position_menu = 4;
	  position_menu = position_menu - 1;
	}

      /* ------------------------
	 Sélection du choix
	 ------------------------*/
      /* Lancer une partie avec espace si on se trouve au bon endroit du menu */
      if ((touche_menu == MLV_KEYBOARD_SPACE) && position_menu % 4 == 0)
	{
	  MLV_clear_window(MLV_COLOR_BLACK);
	  verification_sauvegarde = FALSE;
	  etat_boucle_pseudo = TRUE;
	}
      if ((touche_menu == MLV_KEYBOARD_SPACE) && position_menu % 4 == 1)
	{
	  fichier_sauvegarde = fopen("save.txt", "r");
	  if (fichier_sauvegarde == NULL)
	    {
	      printf("Erreur 1SS: Impossible d'ouvrir le fichier de sauvegarde\n");
	      exit(-1);
	    }
	  if ((lecture_sauvegarde(fichier_sauvegarde, &plateau, pseudo_joueur, &score)) == -1)
	    position_menu = initialisation_menu(image_menu1);
	  else
	    {
	      fclose(fichier_sauvegarde);
	      verification_sauvegarde = TRUE;
	      etat_boucle_pseudo = TRUE;
	    }
	}

      /* Lancer le menu des scores */
      if ((touche_menu == MLV_KEYBOARD_SPACE) && position_menu % 4 == 2)
	{
	  MLV_clear_window(MLV_COLOR_BLACK);
	  etat_boucle_score = TRUE;
	}

      /* Lancer le menu des thèmes */
      if ((touche_menu == MLV_KEYBOARD_SPACE) && position_menu % 4 == 3)
	{
	  MLV_clear_window(MLV_COLOR_BLACK);
	  etat_boucle_theme = TRUE;
	}

      /* ==============================
	 Boucle qui gère les thèmes
	 ==============================*/
      while (etat_boucle_theme == TRUE)
	{
	  /* ------------------------
	     Navigation dans le menu
	     ------------------------*/
	  if (position_theme % 2 == 0)
	    affichage_image(image_theme1);
	  if (position_theme % 2 == 1)
	    affichage_image(image_theme2);

	  MLV_wait_keyboard(&touche_menu_theme, NULL, NULL);
	  if (touche_menu_theme == MLV_KEYBOARD_RIGHT)
	    position_theme = position_theme + 1;
	  if (touche_menu_theme == MLV_KEYBOARD_LEFT)
	    {
	      if (position_theme <= 0)
		position_theme = 2;
	      position_theme = position_theme - 1;
	    }

	  /* ------------------------
	     Sélection du thème
	     ------------------------*/
	  if (touche_menu_theme == MLV_KEYBOARD_SPACE && position_theme % 2 == 0)
	    image_theme_selec = image_theme_base;

	  if (touche_menu_theme == MLV_KEYBOARD_SPACE && position_theme % 2 == 1)
	    image_theme_selec = image_theme_minecraft;

	  /* ------------------------
	     Revenir au menu principal
	     ------------------------*/
	  if (touche_menu_theme == MLV_KEYBOARD_ESCAPE)
	    {
	      position_menu = initialisation_menu(image_menu1);
	      etat_boucle_theme = FALSE;
	    }
	}

      /* ==============================
	 Boucle qui gère le menu des scores
	 ==============================*/
      while (etat_boucle_score == TRUE)
	{
	  MLV_draw_image(image_score, 0, 0);
	  fichier = fopen("save_score.txt", "r+");
	  if (fichier == NULL)
	    {
	      printf("Erreur 1SS: Impossible d'ouvrir le fichier de sauvegarde\n");
	      exit(-1);
	    }
	  score_calcul(fichier);
	  fclose(fichier);

	  /* ------------------------
	     Revenir au menu principal
	     ------------------------*/
	  MLV_wait_keyboard(&touche_menu, NULL, NULL);
	  if (touche_menu == MLV_KEYBOARD_ESCAPE)
	    {
	      position_menu = initialisation_menu(image_menu1);
	      etat_boucle_score = FALSE;
	    }
	}

      /* ==============================
	 Boucle qui gère le pseudo
	 ==============================*/
      while (etat_boucle_pseudo == TRUE)
	{
	  /* Demande le pseudo de la personne */
	  affichage_image(image_pseudo);
	  if (verification_sauvegarde == FALSE)
	    strcpy(pseudo_joueur, ask_pseudo());

	  /* ------------------------
	     Verification du pseudo
	     ------------------------*/

	  /* si le test valide alors on lance le jeu ! */
	  if (verification_pseudo(pseudo_joueur) == 1 || verification_sauvegarde == TRUE)
	    {
	      etat_boucle_pseudo = FALSE;
	      vitesse = 5;
	      if (verification_sauvegarde != TRUE)
		plateau = creer_plateau(HAUT, LONG);
	      //MLV_play_music(music, 2.0, -1);
	      etat_boucle_jeu = TRUE;
	    }
	  /* sinon on redemande le pseudo de la personne en restant dans la boucle*/
	  else
	    {
	      etat_boucle_pseudo = TRUE;
	      etat_boucle_jeu = FALSE;
	    }

	  /* ==============================
	     Boucle de jeu
	     ==============================*/

	  piece_aleatoire = piece_alea();
	  prochaine_piece = piece_alea();
	  placer_piece(piece_aleatoire, &plateau, position_depart, 4);
	  while (etat_boucle_jeu == TRUE)
	    {

	      /* Clear la fenetre avec un fond noir */
	      MLV_clear_window(MLV_COLOR_BLACK);
	      MLV_draw_image(image_theme_selec, 0, 0);
	      MLV_change_frame_rate(FRAME);
	      /* Affichage du plateau de jeu */

	      afficher_plateauMLV(plateau, HAUT, LONG, pseudo_joueur, score);
	      afficher_pro_piece(prochaine_piece);
	      pos_plat_y = 4;
	      n = 0;
	      z = 0;
	      /* Attends que l'utilisateur bouge la pièce (rotation/descendre/mouvement) */
	      while (n == 0)
		{
		  j = 0;
		  MLV_delay_according_to_frame_rate();
		  MLV_actualise_window();
		  afficher_plateauMLV(plateau, HAUT, LONG, pseudo_joueur, score);
		  MLV_get_event(
				&sym, NULL, NULL,
				NULL, NULL,
				NULL, NULL, NULL,
				&state);

		  afficher_pro_piece(prochaine_piece);

		  /* Z permet la rotation de la piece */
		  if (state == MLV_PRESSED && sym == MLV_KEYBOARD_z)
		    piece_aleatoire = rotation_piece(piece_aleatoire, &plateau, position_depart, pos_plat_y);
		  /* =====================
		     PAUSE
		     ====================*/
		  if (state == MLV_PRESSED && sym == MLV_KEYBOARD_SPACE)
		    {
		      /* Affichage image de pause */
		      affichage_image(image_pause);
		      /* Attends la prochaine instruction pour savoir quoi faire */
		      MLV_wait_keyboard(&symp, NULL, NULL);

		      /* V permet la sauvegarde */
		      if (symp == MLV_KEYBOARD_v)
			{ 

			  /* Ouverture du fichier et ecriture de la sauvegarde */
			  fichier_sauvegarde = fopen("save.txt", "w+");
			  if (fichier_sauvegarde == NULL)
			    {
			      printf("Erreur 1SS: Impossible d'ouvrir le fichier de sauvegarde\n");
			      exit(-1);
			    }
			  ecriture_sauvegarde(fichier_sauvegarde, &plateau, score, pseudo_joueur);
			  fclose(fichier_sauvegarde);

			  /* Retour au menu */
			  etat_boucle_jeu = FALSE;
			  /* Stop la musique */
			  //MLV_stop_music();
			  position_menu = initialisation_menu(image_menu1);
			  etat_boucle_menu = TRUE;
			  /* Renitialisation des valeurs du score et de la vitesse */
			  score = 0;
			  vitesse = 5;
			  sym = MLV_KEYBOARD_s;
			  break;
			}
		      /* Espace pour reprendre le jeu */
		      if (symp == MLV_KEYBOARD_SPACE)
			{ 
			  sym = MLV_KEYBOARD_s;
			  afficher_plateau(plateau, HAUT, LONG);
			  j = 1;
			  break;
			}
		      /* Echap pour quitter sans sauvegarder */
		      if (symp == MLV_KEYBOARD_ESCAPE)
			{
			  /* Retour au menu */
			  etat_boucle_jeu = FALSE;
			  //MLV_stop_music();
			  position_menu = initialisation_menu(image_menu1);
			  etat_boucle_menu = TRUE;

			  /* Renitialisation des valeurs du score et de la vitesse */
			  score = 0;
			  vitesse = 5;
			  sym = MLV_KEYBOARD_s;
			  break;
			}
		    }

		  if (state == MLV_PRESSED && sym == MLV_KEYBOARD_LEFT)
		    position_depart = mouvement_piece(piece_aleatoire, &plateau, position_depart, sym, pos_plat_y);
		  if (state == MLV_PRESSED && sym == MLV_KEYBOARD_RIGHT)
		    position_depart = mouvement_piece(piece_aleatoire, &plateau, position_depart, sym, pos_plat_y);

		  /*descendre la piece*/
		  for (i = 0; i < 4; i++)
		    {
		      if (piece_aleatoire.piece[i][0] != 0)
			x1 = i;
		      if (piece_aleatoire.piece[i][1] != 0)
			x2 = i;
		      if (piece_aleatoire.piece[i][2] != 0)
			x3 = i;
		      if (piece_aleatoire.piece[i][3] != 0)
			x4 = i;
		    }
		  if (FRAME / vitesse == images)
		    {
		      if (((plateau.plateau[x1 + z + 1][position_depart + 1] != 0) && (piece_aleatoire.piece[x1][0] != 0) && (x1 != 5)) ||
			  ((plateau.plateau[x2 + z + 1][position_depart + 2] != 0) && (piece_aleatoire.piece[x2][1] != 0) && (x2 != 5)) ||
			  ((plateau.plateau[x3 + z + 1][position_depart + 3] != 0) && (piece_aleatoire.piece[x3][2] != 0) && (x3 != 5)) ||
			  ((plateau.plateau[x4 + z + 1][position_depart + 4] != 0) && (piece_aleatoire.piece[x4][3] != 0) && (x4 != 5)))
			{
			  n = 1;
			  score += 5;
			}
		      else
			{
			  pos_plat_y++;
			  z++;
			}
		      images = 1;
		    }
		  else
		    {
		      images++;
		    }
		  if (j == 0)
		    {
		      placer_piece(piece_aleatoire, &plateau, position_depart, pos_plat_y);
		      afficher_plateauMLV(plateau, HAUT, LONG, pseudo_joueur, score);
		      supprimer_piece(piece_aleatoire, &plateau, position_depart, pos_plat_y);
		    }
		}
	      if (j == 0)
		{
		  placer_piece(piece_aleatoire, &plateau, position_depart, pos_plat_y);
		  position_depart = 4;
		}

	      /* =====================
		 GAME OVER
		 ======================*/
	      if (game_over(&plateau) == TRUE)
		{
		  /* On stop la musique */
		  //MLV_stop_music();

		  /* Sauvegarde du score du joueur */
		  fichier = fopen("save_score.txt", "a+");
		  if (fichier == NULL)
		    {
		      printf("Erreur 1SS: Impossible d'ouvrir le fichier de sauvegarde\n");
		      exit(EXIT_FAILURE);
		    }
		  ecriture_score(fichier, score, pseudo_joueur);
		  fclose(fichier);

		  /* Renitialisation du score et retour au menu */
		  score = 0;
		  position_menu = initialisation_menu(image_menu1);
		  etat_boucle_jeu = FALSE;
		  etat_boucle_menu = TRUE;
		}
	      else
		{
		  score = descendre_jeu(&plateau, score);
		}

	      afficher_plateauMLV(plateau, HAUT, LONG, pseudo_joueur, score);

	      if (j == 0)
		{
		  piece_aleatoire = prochaine_piece;
		  prochaine_piece = piece_alea();
		}

	      afficher_pro_piece(prochaine_piece);
	      placer_piece(piece_aleatoire, &plateau, position_depart, 4);
	      if (score % 100 == 0)
		{
		  vitesse++;
		}
	    }
	}
    }
  /* On supprime la musique et libere le module audio en fin de programme */
  //MLV_free_music(music);
  //MLV_free_audio();
}
