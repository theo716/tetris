#include <stdlib.h>
#include <stdio.h>
#include <MLV/MLV_all.h>
#include "../include/structure.h"
#define NBMAX2 100

/* Ecriture du score dans le fichier en paramètre */
void ecriture_score(FILE *fichier,int score, char* pseudo){
    fprintf(fichier,"%s %d\n",pseudo,score);
}

/* Tri du top 10 des scores */
void score_calcul(FILE *fichier)
{
    /* Utilisation d'une structure joueur pour stocker le pseudo et le score de la personne */
    type_player liste_joueur[10];
    type_player temporaire;
    /* Déclaration des variables */
    int i=0;
    int j,k,l;
    char texte[500];

    /* Initialisation du score des 10 joueurs du top */
    for(l=0;l<10;l++){
        liste_joueur[l].score = 0;
    }

    /* Tri du score à la lecture du fichier */
    while(fscanf(fichier,"%s %d\n",liste_joueur[i].name,&liste_joueur[i].score) == 2 && i<100){
        j=i;
        while( j!= 0 && liste_joueur[j].score > liste_joueur[j-1].score){
            strcpy(temporaire.name,liste_joueur[j-1].name);
            temporaire.score = liste_joueur[j-1].score;

            strcpy(liste_joueur[j-1].name,liste_joueur[j].name);
            liste_joueur[j-1].score = liste_joueur[j].score;
            
            strcpy(liste_joueur[j].name,temporaire.name);
            liste_joueur[j].score = temporaire.score;

            j--;
        }
        i++;
    }

    /* Affichage de vide quand le score = 0 */
    for(k=0;k<10;k++){
        if(liste_joueur[k].score == 0){
            sprintf(texte, "%d.vide", k+1);
            MLV_draw_text(420,50*(k+6), texte, MLV_COLOR_ALICE_BLUE);
        }

        /* Sinon affichage du pseudo et du score du joueur */
        else{
            sprintf(texte, "%d. %s %d", k+1, liste_joueur[k].name,liste_joueur[k].score);
            MLV_draw_text(420,50*(k+6), texte, MLV_COLOR_ALICE_BLUE);
        }
    }
    MLV_actualise_window();
}