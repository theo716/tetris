#include <stdio.h>
#include <stdlib.h>
#include "../include/structure.h"
#define TAILLEMAX 10

/* Ecriture de la sauvegarde */
void ecriture_sauvegarde(FILE *fichier, type_plateau *plateau, int score, char* pseudo){
  int i,j;

  /* Ecriture du pseudo et du score dans le format suivant: pseudo score */
  fprintf(fichier,"%s %d\n",pseudo,score);

  /* Recopie du plateau de jeu ligne par ligne */
  for(i=0;i<23;i++){
    for(j=0;j<11;j++){
      fprintf(fichier,"%d ",plateau->plateau[i][j]);
    }
    fprintf(fichier,"\n");
  }
}

/* Fonction lecture sauvegarde */
int lecture_sauvegarde(FILE *fichier,type_plateau *plateau,char*pseudo,int *score){
  int i,j;

  /* Lecture du pseudo et du score */
  if((fscanf(fichier,"%s %d\n",pseudo,score)) != 2){
    printf("Erreur 1 sauvegarde.c : impossible de lire le pseudo ou le score.\n");
    return -1;
  }
  else{

    /* Lecture du plateau de jeu ligne par ligne */
    for(i=0;i<23;i++){
      for(j=0;j<11;j++){
	if((fscanf(fichier,"%hd  ",&plateau->plateau[i][j])) != 1){
	  printf("Erreur 2 sauvegarde.c : impossible de lire la matrice du plateau\n");
	  return -1;
	}
      }
    }
  }
  return 0;
}
  
