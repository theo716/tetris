void placer_piece(type_piece piece,type_plateau *plateau, int x, int y);
int mouvement_piece(type_piece piece, type_plateau *plateau,int n, MLV_Keyboard_button touche,int position_y);
type_piece rotation_piece(type_piece piece,type_plateau *plateau, int x, int y);
int descendre_piece(type_piece piece , type_plateau *plateau,int x,int score);
type_piece piece_alea ();
int descendre_jeu(type_plateau *plateau,int score);
int game_over(type_plateau *plateau);
void supprimer_piece(type_piece piece,type_plateau *plateau, int x, int y);
void afficher_pro_piece(type_piece piece);

