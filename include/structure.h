#define NBMAX 25
#define NBMAXPSEUDO 40

typedef struct {
  short plateau[NBMAX][NBMAX];
}type_plateau;

typedef struct {
  int piece[4][4];
}type_piece;

typedef struct {
  char name[NBMAXPSEUDO];
  int score;
}type_player;